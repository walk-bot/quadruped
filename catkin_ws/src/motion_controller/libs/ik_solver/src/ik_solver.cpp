#include "ik_solver/ik_solver.h"

#include <math.h>

#include <eigen3/Eigen/Geometry>
#include <vector>

#include "specs.h"

Vector3 ik_solver::leg_inverse(const Eigen::Vector4f &p) {
    const double f = sqrt(pow(p[0], 2) + pow(p[1], 2) - pow(l1, 2));

    const double g = f - l2;
    const double h = sqrt(pow(g, 2) + pow(p[2], 2));
    const double d1 = pow(h, 2) - pow(l3, 2) - pow(l4, 2);
    const double d2 = 2 * l3 * l4;
    const double d = d1 / d2;

    const double theta3 = acos(d);

    return {
        .x = -atan2(p[1], p[0]) - atan2(f, -l1),
        .y = atan2(p[2], g) - atan2(l4 * sin(theta3), l3 + l4 * cos(theta3)),
        .z = theta3};
}

Eigen::Vector4f ik_solver::leg_forward(const double &t1, const double &t2,
                                       const double &t3) {
    const float x = -l1 * cos(t1) - l2 * sin(t1) - l3 * sin(t1) * cos(t2) -
                    l4 * sin(t1) * cos(t2 + t3);
    const float y = l1 * sin(t1) - l2 * cos(t1) - l3 * cos(t1) * cos(t2) -
                    l4 * cos(t1) * cos(t2 + t3);
    const float z = l3 * sin(t2) + l4 * sin(t2 + t3);

    Eigen::Vector4f res = {x, y, z, 1};

    return res;
}

void ik_solver::convert(Vector3 &a, const bool &left) {
    a.x = (left) ? a.x : -a.x;
    a.y = -a.y - M_PI_2;
    a.z = -a.z + M_PI_2;
}

// calculate the transformation matrices to convert from world frame to specific
// leg frames
void ik_solver::setTransformationMatrices() {
    Eigen::Matrix4f rx;
    rx << 1, 0, 0, 0, 0, cos(rpy.x), -sin(rpy.x), 0, 0, sin(rpy.x), cos(rpy.x),
        0, 0, 0, 0, 1;

    Eigen::Matrix4f ry;
    ry << cos(rpy.y), 0, sin(rpy.y), 0, 0, 1, 0, 0, -sin(rpy.y), 0, cos(rpy.y),
        0, 0, 0, 0, 1;

    Eigen::Matrix4f rz;
    rz << cos(rpy.z), -sin(rpy.z), 0, 0, sin(rpy.z), cos(rpy.z), 0, 0, 0, 0, 1,
        0, 0, 0, 0, 1;

    // 3d rotation matrix in upper left corner
    Eigen::Matrix4f rxyz = rx * ry * rz;

    Eigen::Matrix4f T;
    T << 0, 0, 0, midpoint.x, 0, 0, 0, midpoint.z, 0, 0, 0, midpoint.y, 0, 0, 0,
        0;

    Eigen::Matrix4f Tm = T + rxyz;

    Eigen::Matrix4f lf;
    lf << cos(M_PI / 2), 0, sin(M_PI / 2), robotLength / 2, 0, 1, 0, 0,
        -sin(M_PI / 2), 0, cos(M_PI / 2), robotWidth / 2, 0, 0, 0, 1;

    Eigen::Matrix4f lb;
    lb << cos(M_PI / 2), 0, sin(M_PI / 2), -robotLength / 2, 0, 1, 0, 0,
        -sin(M_PI / 2), 0, cos(M_PI / 2), robotWidth / 2, 0, 0, 0, 1;

    Eigen::Matrix4f rb;
    rb << cos(M_PI / 2), 0, sin(M_PI / 2), -robotLength / 2, 0, 1, 0, 0,
        -sin(M_PI / 2), 0, cos(M_PI / 2), -robotWidth / 2, 0, 0, 0, 1;

    Eigen::Matrix4f rf;
    rf << cos(M_PI / 2), 0, sin(M_PI / 2), robotLength / 2, 0, 1, 0, 0,
        -sin(M_PI / 2), 0, cos(M_PI / 2), -robotWidth / 2, 0, 0, 0, 1;

    // set transformation matrices for shoulders and the midpoint
    Tlf = Tm * lf;
    Tlb = Tm * lb;
    Trb = Tm * rb;
    Trf = Tm * rf;

    // on the right side of robot we have to flip x coordinate to use same
    // inverse trig math as left side
    Ix << -1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1;
}

std::vector<double> ik_solver::getAngles(const std::vector<Vector3> &fpos) {
    Eigen::Vector4f lf;
    lf << fpos[0].x, fpos[0].z, fpos[0].y, 1;
    Eigen::Vector4f lb;
    lb << fpos[1].x, fpos[1].z, fpos[1].y, 1;
    Eigen::Vector4f rb;
    rb << fpos[2].x, fpos[2].z, fpos[2].y, 1;
    Eigen::Vector4f rf;
    rf << fpos[3].x, fpos[3].z, fpos[3].y, 1;

    // Convert from world space into leg space coordinates
    Eigen::Vector4f lf_p_legspace = Tlf.inverse() * lf;
    // calculate the leg_angles based on leg space coordinates
    Vector3 lf_angles = leg_inverse(lf_p_legspace);
    convert(lf_angles, true);

    Eigen::Vector4f lb_p_legspace = Tlb.inverse() * lb;
    Vector3 lb_angles = leg_inverse(lb_p_legspace);
    convert(lb_angles, true);

    Eigen::Vector4f rb_p_legspace = Ix * Trb.inverse() * rb;
    Vector3 rb_angles = leg_inverse(rb_p_legspace);
    convert(rb_angles, false);

    Eigen::Vector4f rf_p_legspace = Ix * Trf.inverse() * rf;
    Vector3 rf_angles = leg_inverse(rf_p_legspace);
    convert(rf_angles, false);

    return {
        lf_angles.x, lf_angles.y, lf_angles.z, lb_angles.x,
        lb_angles.y, lb_angles.z, rb_angles.x, rb_angles.y,
        rb_angles.z, rf_angles.x, rf_angles.y, rf_angles.z,
    };
}

std::vector<Vector3> ik_solver::getPoints(const std::vector<double> &q) {
    Vector3 qlf = {.x = q[0], .y = q[1], .z = q[2]};
    Vector3 qlb = {.x = q[3], .y = q[4], .z = q[5]};
    Vector3 qrb = {.x = q[6], .y = q[7], .z = q[8]};
    Vector3 qrf = {.x = q[9], .y = q[10], .z = q[11]};

    convert(qlf, true);
    convert(qlb, true);
    convert(qrb, false);
    convert(qrf, false);

    Eigen::Vector4f lf = Tlf * leg_forward(qlf.x, qlf.y, qlf.z);
    Eigen::Vector4f lb = Tlb * leg_forward(qlb.x, qlb.y, qlb.z);
    Eigen::Vector4f rb = Trb * Ix * leg_forward(qrb.x, qrb.y, qrb.z);
    Eigen::Vector4f rf = Trf * Ix * leg_forward(qrf.x, qrf.y, qrf.z);

    return {
        {.x = lf[0], .y = lf[2], .z = lf[1]},
        {.x = lb[0], .y = lb[2], .z = lb[1]},
        {.x = rb[0], .y = rb[2], .z = rb[1]},
        {.x = rf[0], .y = rf[2], .z = rf[1]},
    };
}