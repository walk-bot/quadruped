# Sensor Feedback

For closed loop control, we need to know the robots state. This includes knowing its attitude and joint positions. This information will be read from a Teensy 3.5, which has a small footprint and has over 64 GPIO pins from which we can read from sensors in the robot. The wires are directly soldered to the Teensy, because otherwise they become unseated and lose connection if they are plugged into a breadboard.

Joint positions are given by the internal potentiometer values of the hobby servos (12 analog values for 12 servos). These are internally used for position PID control by the servos and not normally exposed to the user, but can be read by soldering a fourth wire to the servos control board. The potentionmeter is just a variable voltage divider proportional to the angular displacement of the rotor. We can use the voltage to calculate joint angles. This is both cheap and fairly easy, because I dont have to buy new joint encoders and mount them anywhere, increasing the number of parts.

Before soldering, you should verify whether you are soldering the wire to the correct spot with a multimeter.

|    Fourth Wire to Potentiometer    |           Teensy in Robot            |
| :--------------------------------: | :----------------------------------: |
| ![Wire](./img/20220213_172955.jpg) | ![Teensy](./img/20220214_132828.jpg) |

Next up we will be publishing a `sensor_msgs::Imu` message from the following IMU ([Adafruit Precision NXP 9-DOF Breakout Board - FXOS8700 + FXAS21002](https://learn.adafruit.com/nxp-precision-9dof-breakout)). It needs to be calibrated according to the tutorial in the document and is configured to integrate and filter the attitude on the teensy.

The original version of MotionCal from PJRC is outdated and was updated to be built on Ubuntu 20.04 by this guys github fork.

```sh
git clone https://github.com/henrythasler/MotionCal
```

The Teensy is linked to the rest of ROS using Rosserial_Arduino, which allows you to set up a node with publishers and subscribers with basically no difference to normal roscpp.

## Microcontroller Software

Install Platformio to program the Teensy. This is an "extended version" of the arduino IDE, which is to say that it requires almost no manual configuration after a fresh install. The project contains all the information for libraries and dependencies, which are automatically installed by pio. You also use regular c++ instead of the Arduino version. Select the project in sensor-fb, plug in the Teensy over USB and click on build. Simple as that.

## Rosserial

The Teensy uses this to send data over to the rest of the system. Make sure your computer has the appropriate software ready to receive the data.

```sh
sudo apt-get install  ros-noetic-rosserial ros-noetic-rosserial-arduino
```
The publishers on the microcontroller are connected to ROS using the following command.

```sh
rosrun rosserial_python serial_node.py /dev/ttyACM0
```