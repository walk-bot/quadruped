
# This file is meant to be run on the Raspberry pi. Send the joystick info using the following command:
# Install joy using sudo apt install ros-$ROS_DISTRO-joy
# rosrun joy joy_node

from typing import List
import rospy
import busio
import numpy as np
import os
import time

from sklearn.linear_model import LinearRegression
from sensor_msgs.msg import Joy
from std_msgs.msg import Int16MultiArray

from board import SCL, SDA
from adafruit_pca9685 import PCA9685

# encoding for the position we currently measured. 0 =^ -90 deg; 1 =^ 0 deg; 2 =^ 90 deg
position_index: int = 0
joint_index: int = 0  # The joint which is currently being manipulated
potentiometer_values: List = [
    0, 0, 0,
    0, 0, 0,
    0, 0, 0,
    0, 0, 0
]

i2c_bus = busio.I2C(SCL, SDA)
pca = PCA9685(i2c_bus, address=0x40)
pca.frequency = 330

pi: float = 3.1415926


class Joint():
    def __init__(self, lim_lower: float, lim_upper: float, name: str):
        self.limits = [lim_lower, 0, lim_upper]
        self.name = name

    name: str = ""
    # measured pulse width from 0 to 4096 to make servo go to +90, 0, -90
    pw: List = [650, 1900, 3200]

    # integer "intensity" from 0 to 2048 of potentiometer at 0 and 180 degrees
    pot: List = [100, 1000, 2000]

    # physical joint limits in radians
    limits = [0, 0, 0]

    # linear regression coefficients for angular displacement to pulse width
    # f(x) = slope_pw * x + intercept_pw
    slope_pw: float = 0.0
    intercept_pw: float = 0.0

    # same principle but now for the potentiometer back to the angular displacement
    slope_pot: float = 0.0
    intercept_pot: float = 0.0


joints: List = [
    Joint(-0.55, pi / 2, "front_left_shoulder"),
    Joint(-pi / 2, pi / 2, "front_left_leg"),
    Joint(-pi / 2, pi / 2, "front_left_foot"),

    Joint(-0.55, pi / 2, "rear_left_shoulder"),
    Joint(-pi / 2, pi / 2, "rear_left_leg"),
    Joint(-pi / 2, pi / 2, "rear_left_foot"),

    Joint(-pi / 2, 0.55, "rear_right_shoulder"),
    Joint(pi / 2, -pi / 2, "rear_right_leg"),
    Joint(-pi / 2, pi / 2, "rear_right_foot"),

    Joint(-pi / 2, 0.55, "front_right_shoulder"),
    Joint(-pi / 2, pi / 2, "front_right_leg"),
    Joint(-pi / 2, pi / 2, "front_right_foot"),
]


def handleDPad(cross: int):
    if cross < 0:
        switchJoint(1)
    elif cross > 0:
        switchJoint(-1)


def pulse_width(x: int) -> int:
    min_pw = 500  # empirically determined
    max_pw = 3600

    slope = (max_pw - min_pw) / 2  # 2 because 2 is domain
    intercept = min_pw + slope

    return int(slope * x + intercept)


def switchJoint(value):
    global joint_index

    joint_index += value

    # wrap around when first and last are reached
    if joint_index > 11:
        joint_index = 0
    if joint_index < 0:
        joint_index = 11

    rospy.loginfo("Calibrating joint index:\t%d ", joint_index)


def saveMeasurement(pulse: int):
    rospy.loginfo("Saving value for joint %d at position %d",
                  joint_index, joints[joint_index].limits[position_index] * 180 / pi)

    # because lists are mutable (pointers) you have to make a copy of the list and then reassign it.
    # Dont ask why. I should get a book about python
    pws = joints[joint_index].pw.copy()
    pots = joints[joint_index].pot.copy()

    pws[position_index] = pulse
    pots[position_index] = potentiometer_values[joint_index]

    joints[joint_index].pw = pws
    joints[joint_index].pot = pots


def setNextMeasurementPosition():
    global position_index

    position_index += 1
    if position_index > 2:
        position_index = 0


def calculate_coefficients():
    rospy.loginfo("Calculating coefficients for all joints")
    for i in range(len(joints)):
        joint: Joint = joints[i]

        rospy.loginfo(joint.pw)
        rospy.loginfo(joint.pot)

        angles = np.array([joint.limits[0], joint.limits[1],
                          joint.limits[2]]).reshape((-1, 1))
        pulse_widths = np.array([joint.pw[0], joint.pw[1], joint.pw[2]])
        pot_values = np.array(
            [joint.pot[0], joint.pot[1], joint.pot[2]])

        pw_fit = LinearRegression().fit(angles, pulse_widths)
        pot_fit = LinearRegression().fit(angles, pot_values)

        joint.slope_pw = pw_fit.coef_[0]
        joint.intercept_pw = pw_fit.intercept_

        joint.slope_pot = pot_fit.coef_[0]
        joint.intercept_pot = pot_fit.intercept_


def saveResultsToFile():
    path = os.environ['CATKIN_WS']
    with open(path + '/src/servo_driver/calibration.yaml', 'w') as file:
        file.write("joints:\n")
        for i in range(12):
            joint = joints[i]

            file.write("  j" + str(i) + ":\n")
            file.write("    name: " + joint.name + "\n")
            file.write("    lim_lower: " +
                       str(joint.limits[0]) + "\n")
            file.write("    lim_upper: " +
                       str(joint.limits[2]) + "\n")

            file.write("    slope_pw: " + str(joint.slope_pw) + "\n")
            file.write("    intercept_pw: " + str(joint.intercept_pw) + "\n")
            file.write("    slope_pot: " + str(joint.slope_pot) + "\n")
            file.write("    intercept_pot: " + str(joint.intercept_pot) + "\n")
            file.write("\n")


timestamp = time.time() * 1000


def joystick_callback(msg):
    global joint_index
    global position_index
    global timestamp

    joystick_displacement = msg.axes[1]
    cross = msg.axes[6]
    A_pressed = msg.buttons[0]
    B_pressed = msg.buttons[1]

    handleDPad(cross)
    pulse: int = pulse_width(joystick_displacement)
    rospy.loginfo("%d \t %d \t %d", joint_index,
                  joints[joint_index].limits[position_index] * 180 / pi,  pulse)

    pca.channels[joint_index].duty_cycle = pulse * 16

    # only allow button press every second
    if ((time.time() * 1000 - timestamp) > 1000):
        if(A_pressed and not B_pressed):
            timestamp = time.time()
            saveMeasurement(pulse)
            setNextMeasurementPosition()

        if(B_pressed and not A_pressed):
            timestamp = time.time()
            calculate_coefficients()
            saveResultsToFile()


def pot_callback(msg):
    global potentiometer_values
    potentiometer_values = msg.data


def main():
    welcomemesg: str = """
    ::::::::::::::::: Calibrating Joints :::::::::::::::::::
    Cycle through joints with the D-Pad.\n
    Set measurements for given angles using the A button.\n
    Calculate coefficients and write to file using B button\n\n"""

    rospy.init_node("calibrate_servo", anonymous=True)
    rospy.Subscriber('joy', Joy, joystick_callback, queue_size=1)
    rospy.Subscriber(
        '/quad/joint_position_feedback', Int16MultiArray, pot_callback, queue_size=1)

    rospy.loginfo(welcomemesg)

    rospy.spin()


if __name__ == "__main__":
    main()
