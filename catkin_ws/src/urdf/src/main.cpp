#include <ros/ros.h>
#include <sensor_msgs/Imu.h>
#include <sensor_msgs/JointState.h>
#include <std_msgs/Int16MultiArray.h>
#include <tf/transform_broadcaster.h>

#include <string>
#include <vector>

sensor_msgs::JointState joint_state;
geometry_msgs::TransformStamped odom_trans;

struct Joint {
    Joint(float i, float s, std::string n) {
        intercept = i;
        slope = s;
        name = n;
    }

    float intercept, slope;
    std::string name;
};

std::vector<Joint> joints = {};

void loadParams(ros::NodeHandle& n) {
    float intercept = 0;
    float slope = 0;
    std::string name;

    for (ushort i = 0; i < 12; i++) {
        std::string key = "/joints/j" + std::to_string(i);

        n.getParam(key + "/slope_pot", slope);
        n.getParam(key + "/intercept_pot", intercept);
        n.getParam(key + "/name", name);

        joints.push_back(Joint(intercept, slope, name));
    }
}

void onJointAngleReceived(const std_msgs::Int16MultiArray::ConstPtr& msg) {
    joint_state.header.stamp = ros::Time::now();
    joint_state.name.resize(joints.size());
    joint_state.position.resize(joints.size());

    for (ushort i = 0; i < joints.size(); i++) {
        float data = (float)msg->data[i];
        float pos_deg = (data - joints[i].intercept) /
                        joints[i].slope;  // y = ax + b <=> x = (y - b) / a
        joint_state.name[i] = joints[i].name;
        joint_state.position[i] = pos_deg;
    }
}

void onImuMsgReceived(const sensor_msgs::Imu::ConstPtr& msg) {
    tf2::Quaternion rotation;
    odom_trans.header.stamp = ros::Time::now();
    odom_trans.transform.translation.x = 0;
    odom_trans.transform.translation.y = 0;
    odom_trans.transform.translation.z = 0.15;

    odom_trans.transform.rotation.x = msg->orientation.x;
    odom_trans.transform.rotation.y = msg->orientation.y;
    odom_trans.transform.rotation.z = msg->orientation.z;
    odom_trans.transform.rotation.w = msg->orientation.w;
}

int main(int argc, char** argv) {
    ros::init(argc, argv, "listener");
    ros::NodeHandle n;
    ros::Rate cycleFreq(60);

    ros::Subscriber joint_sub = n.subscribe("/quad/joint_position_feedback",
                                            1000, onJointAngleReceived);
    ros::Subscriber imu_sub = n.subscribe("/quad/imu", 1000, onImuMsgReceived);
    ros::Publisher joint_state_pub =
        n.advertise<sensor_msgs::JointState>("joint_states", 1);
    tf::TransformBroadcaster broadcaster;

    odom_trans.header.frame_id = "odom";
    odom_trans.child_frame_id = "dummy";

    loadParams(n);

    while (ros::ok()) {
        ros::spinOnce();

        joint_state_pub.publish(joint_state);
        broadcaster.sendTransform(odom_trans);

        cycleFreq.sleep();
    }

    return 0;
}