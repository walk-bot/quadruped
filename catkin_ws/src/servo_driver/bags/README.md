

First record messages
```sh
rosbag record /quad/servo_angles
```

then decode the data as CSV
```sh
rostopic echo -b 2022*.bag -p /quad/servo_angles >> msgs.csv
```