#include "motion_controller.h"

#include <geometry_msgs/Twist.h>
#include <math.h>
#include <ros/ros.h>
#include <sensor_msgs/JointState.h>
#include <std_msgs/Float64MultiArray.h>
#include <tf/transform_datatypes.h>
#include <visualization_msgs/MarkerArray.h>

#include "ik_solver/ik_solver.h"
#include "models.h"

#define FREQ 60  // in Hz, defines the refresh rate

/**
 * @brief The motion controller subscribes to the cmd_vel topic and translates
 * this into servo positions.
 */
int main(int argc, char** argv) {
    ros::init(argc, argv, "state_publisher");
    ros::NodeHandle nh;
    ros::Rate cycleFreq(FREQ);

    motion_controller mc(nh);

    while (ros::ok()) {
        mc.nextTick();
        ros::spinOnce();
        cycleFreq.sleep();
    }

    return 0;
}

motion_controller::motion_controller(ros::NodeHandle nh) {
    nh_ = nh;
    odom_trans.header.frame_id = "odom";
    odom_trans.child_frame_id = "base_link";

    joint_pub = nh_.advertise<sensor_msgs::JointState>("joint_states", 1);
    servo_pub = nh_.advertise<std_msgs::Float64MultiArray>(
        "/quad/joint_positions", FREQ);
    vis_pub = nh_.advertise<visualization_msgs::MarkerArray>(
        "visualization_marker_array", 1);

    joint_sub = nh_.subscribe("/quad/joint_position_feedback", 10,
                              &motion_controller::onJointAngleReceived, this);
    imu_sub = nh_.subscribe("/quad/imu", 10,
                            &motion_controller::onImuMsgReceived, this);
    odom_sub = nh_.subscribe("/quad/odom", 10,
                             &motion_controller::onOdomReceived, this);
    vel_sub =
        nh_.subscribe("/joy/cmd_vel", 10, &motion_controller::onCmdVel, this);
}

/**
 * @brief update internal orientation vector with feedback from IMU
 */
void motion_controller::onImuMsgReceived(
    const sensor_msgs::Imu::ConstPtr& imu_msg) {
    tf::Quaternion q;
    tf::quaternionMsgToTF(imu_msg->orientation, q);

    tf::Matrix3x3(q).getRPY(rpy.x, rpy.y, rpy.z);
}

/**
 * @brief update internal vector of joint positions for forward kinematics
 */
void motion_controller::onJointAngleReceived(
    const std_msgs::Float64MultiArray::ConstPtr& q_msg) {
    q_act = q_msg->data;
}

void motion_controller::onOdomReceived(
    const geometry_msgs::Point::ConstPtr& p) {
    displacement = {
        .x = p->x,
        .y = p->y,
        .z = p->z,
    };
}

/**
 * @brief take controller input vector and scale with constant factor
 */
void motion_controller::onCmdVel(const geometry_msgs::Twist::ConstPtr& twst) {
    baseLinkTorsor.v = {.x = twst->linear.x * max_vel.v.x,
                        .y = twst->linear.y * max_vel.v.y,
                        .z = twst->linear.z * max_vel.v.z};

    vel_norm = sqrt(pow(twst->linear.x, 2) + pow(twst->linear.y, 2) +
                    pow(twst->linear.z, 2));

    baseLinkTorsor.w = {.x = twst->angular.x * max_vel.w.x,
                        .y = twst->angular.y * max_vel.w.y,
                        .z = twst->angular.z * max_vel.w.z};
}

void motion_controller::nextTick() {
    setDisplacement();
    setShoulders();

    // t should equal 1 after half of step time
    t += (2.0 / (t_step * FREQ));

    if (t >= 1) {
        t = 0;
        ctr++;

        for (size_t leg = 0; leg < flightPhases.size(); leg++) {
            moveLeg(leg, ctr);
        }
    }

    fpos = bezier(t);

    for (size_t leg = 0; leg < fpos.size(); leg++) {
        setMarker(fpos[leg], 0xFFFFFF);
        setMarker(shoulders[leg], 0xff0083);

        setMarker(s0[leg], 0xCC2222);
        setMarker(s1[leg], 0x22CC6B);
        setMarker(s2[leg], 0xCCC822);

        setMarker(displacement, 0xFF00FF);
    }

    calculateServoAngles();
    publish();
}

/**
 * do a givens rotation around z axis
 * [cos(a) -sin(a) 0; sin(a) cos(a) 0; 0 0 1] * [x, y, z]
 */
Vector3 motion_controller::rotate_z(const Vector3& v, const double& phi) {
    return {.x = cos(phi) * v.x - sin(phi) * v.y,
            .y = sin(phi) * v.x + cos(phi) * v.y,
            .z = v.z};
}

/**
 * @brief set position vector of the base link
 */
void motion_controller::setDisplacement() {
    // only regard angular displacement around z axis (yaw)
    rpy += baseLinkTorsor.w * (1.0 / FREQ);

    // rotate velocity vector
    displacement += rotate_z(baseLinkTorsor.v, rpy.z) * (1.0 / FREQ);
}

/**
 * @brief set indexed leg in flight or in stanceo
 * find the target- and midpoints
 */
void motion_controller::moveLeg(const size_t& l, const uint& phase) {
    // set starting point to the previous target point
    s0[l] = s2[l];

    // case where l is supposed to move
    if (flightPhases[l][phase % 4]) {
        Vector3 proj = {.x = shoulders[l].x, .y = shoulders[l].y, .z = 0};
        s2[l] = proj + (rotate_z(baseLinkTorsor.v, rpy.z) * 0.5 * t_step);

        ROS_INFO("%f", (s0[l] - proj).norm());

        // check if foot is already centered
        if ((s0[l] - proj).norm() < 0.003) {
            // then don't lift foot, so feet dont bob around unnecessarily
            s1[l] = s0[l];
        } else {
            // Midpoint of trajectory is above middle of connecting vector s2-s0
            s1[l] = s0[l] + ((s2[l] - s0[l]) * 0.5) + stepHeight;
        }

    } else {
        // else l will be in stance, don't lift foot
        s1[l] = s0[l];
    }
}

void motion_controller::setShoulders() {
    shoulders = initPosition;

    for (Vector3& v : shoulders) {
        // first get shoulder pointing in correct direction around origin
        v = rotate_z(v, rpy.z);

        // then translate to actual place in space
        v += displacement;
    }
}

/**
 * @brief generate a trajectory using 3 control points
 *
 * @param double t in closed interval [0, 1]
 * @see
 * https://andybrown.me.uk/2010/12/05/animation-on-the-arduino-with-easing-functions/
 * @see https://gist.github.com/zeffii/c1e14dd6620ad855d81ec2e89a859719
 * @see https://javascript.info/bezier-curve
 */
const FPos motion_controller::bezier(double t) {
    // use polynomial easing to make movements more fluid and reduce joint
    // acceleration

    // cubic ease in / out:
    if (t < 0.5) {
        t = 4 * pow(t, 3);
    } else {
        double f = ((2 * t) - 2);
        t = 0.5 * pow(f, 3) + 1;
    }

    // generate final point along curve
    return {
        (s0[0] * pow(1 - t, 2)) + (s1[0] * 2 * (1 - t) * t) +
            (s2[0] * pow(t, 2)),
        (s0[1] * pow(1 - t, 2)) + (s1[1] * 2 * (1 - t) * t) +
            (s2[1] * pow(t, 2)),
        (s0[2] * pow(1 - t, 2)) + (s1[2] * 2 * (1 - t) * t) +
            (s2[2] * pow(t, 2)),
        (s0[3] * pow(1 - t, 2)) + (s1[3] * 2 * (1 - t) * t) +
            (s2[3] * pow(t, 2)),
    };
}

void motion_controller::calculateServoAngles() {
    ik_solver ik(rpy, displacement);
    servo_angles.clear();

    servo_angles = ik.getAngles(fpos);
    std::vector<Vector3> points = ik.getPoints(servo_angles);

    ROS_INFO("lf: %f %f %f", points[0].x, points[0].y, points[0].z);
    ROS_INFO("lb: %f %f %f", points[1].x, points[1].y, points[1].z);
    ROS_INFO("rb: %f %f %f", points[2].x, points[2].y, points[2].z);
    ROS_INFO("rf: %f %f %f", points[3].x, points[3].y, points[3].z);

    setMarker({.x = points[0].x, .y = points[0].y, .z = points[0].z}, 0x2596be);
    setMarker({.x = points[1].x, .y = points[1].y, .z = points[1].z}, 0x2596be);
    setMarker({.x = points[2].x, .y = points[2].y, .z = points[2].z}, 0x2596be);
    setMarker({.x = points[3].x, .y = points[3].y, .z = points[3].z}, 0x2596be);
}

void motion_controller::publish() {
    std_msgs::Float64MultiArray sa_msg = servoAngleMsg();
    servo_pub.publish(sa_msg);

    setJointState();
    joint_pub.publish(joint_state);

    setOdometry();
    broadcaster.sendTransform(odom_trans);

    vis_pub.publish(markers);
    markers.markers.clear();
}

std_msgs::Float64MultiArray motion_controller::servoAngleMsg() {
    std_msgs::Float64MultiArray sa_msg;

    sa_msg.layout.dim.push_back(std_msgs::MultiArrayDimension());
    sa_msg.layout.dim[0].size = servo_angles.size();
    sa_msg.layout.dim[0].stride = 1;
    sa_msg.layout.dim[0].label = "quad/servo_angles";

    // Copy in the data
    sa_msg.data.insert(sa_msg.data.end(), servo_angles.begin(),
                       servo_angles.end());
    return sa_msg;
}

void motion_controller::setJointState() {
    joint_state.header.stamp = ros::Time::now();

    joint_state.name.resize(12);
    joint_state.position.resize(12);
    joint_state.name[0] = "front_left_shoulder";
    joint_state.position[0] = servo_angles[0];
    joint_state.name[1] = "front_left_leg";
    joint_state.position[1] = servo_angles[1];
    joint_state.name[2] = "front_left_foot";
    joint_state.position[2] = servo_angles[2];

    joint_state.name[3] = "rear_left_shoulder";
    joint_state.position[3] = servo_angles[3];
    joint_state.name[4] = "rear_left_leg";
    joint_state.position[4] = servo_angles[4];
    joint_state.name[5] = "rear_left_foot";
    joint_state.position[5] = servo_angles[5];

    joint_state.name[6] = "rear_right_shoulder";
    joint_state.position[6] = servo_angles[6];
    joint_state.name[7] = "rear_right_leg";
    joint_state.position[7] = servo_angles[7];
    joint_state.name[8] = "rear_right_foot";
    joint_state.position[8] = servo_angles[8];

    joint_state.name[9] = "front_right_shoulder";
    joint_state.position[9] = servo_angles[9];
    joint_state.name[10] = "front_right_leg";
    joint_state.position[10] = servo_angles[10];
    joint_state.name[11] = "front_right_foot";
    joint_state.position[11] = servo_angles[11];
}
void motion_controller::setOdometry() {
    tf2::Quaternion rotation;
    odom_trans.header.stamp = ros::Time::now();
    odom_trans.transform.translation.x = displacement.x;
    odom_trans.transform.translation.y = displacement.y;
    odom_trans.transform.translation.z = displacement.z;

    rotation.setRPY(rpy.x, rpy.y, rpy.z);
    rotation = rotation.normalize();

    odom_trans.transform.rotation.x = rotation.getX();
    odom_trans.transform.rotation.y = rotation.getY();
    odom_trans.transform.rotation.z = rotation.getZ();
    odom_trans.transform.rotation.w = rotation.getW();
}

void motion_controller::setMarker(const Vector3& v, const int& rgb) {
    visualization_msgs::Marker marker;
    marker.header.frame_id = "odom";
    marker.header.stamp = ros::Time();
    marker.ns = "quadruped_markers";
    marker.id = markers.markers.size() + 1;

    marker.type = visualization_msgs::Marker::SPHERE;
    marker.action = visualization_msgs::Marker::ADD;

    marker.pose.position.x = v.x;
    marker.pose.position.y = v.y;
    marker.pose.position.z = v.z;

    marker.pose.orientation.x = 0.0;
    marker.pose.orientation.y = 0.0;
    marker.pose.orientation.z = 0.0;
    marker.pose.orientation.w = 1.0;

    marker.scale.x = 0.01;
    marker.scale.y = 0.01;
    marker.scale.z = 0.01;

    marker.color.a = 1.0;
    marker.color.r = ((rgb >> 16) & 0xFF) / 255.0;
    marker.color.g = ((rgb >> 8) & 0xFF) / 255.0;
    marker.color.b = ((rgb)&0xFF) / 255.0;

    markers.markers.push_back(marker);
}
