#include "joystick_controller.h"

#include <geometry_msgs/Twist.h>
#include <ros/ros.h>
#include <sensor_msgs/Joy.h>

#include <iostream>

using namespace std;

joystick_controller::joystick_controller()
    : roll_(5), pitch_(4), yaw_(3), x_(0), y_(1), z_(2) {
    nh_.param("axis_roll", roll_, roll_);
    nh_.param("axis_pitch", pitch_, pitch_);
    nh_.param("axis_yaw", yaw_, yaw_);

    nh_.param("axis_x", x_, x_);
    nh_.param("axis_y", y_, y_);
    nh_.param("axis_z", z_, z_);

    nh_.param("scale_angular", a_scale_, a_scale_);
    nh_.param("scale_linear", l_scale_, l_scale_);

    vel_pub_ = nh_.advertise<geometry_msgs::Twist>("/joy/cmd_vel", 1);
    joy_sub_ = nh_.subscribe<sensor_msgs::Joy>(
        "joy", 10, &joystick_controller::joyCallback, this);
};

void joystick_controller::joyCallback(const sensor_msgs::Joy::ConstPtr& joy) {
    geometry_msgs::Twist twist;
    twist.angular.x = a_scale_ * joy->axes[roll_];
    twist.angular.z = a_scale_ * joy->axes[pitch_];
    twist.angular.z = a_scale_ * joy->axes[yaw_];

    twist.linear.x = l_scale_ * joy->axes[x_];
    twist.linear.y = l_scale_ * joy->axes[y_];
    twist.linear.z = l_scale_ * joy->axes[z_];
    vel_pub_.publish(twist);
}

int main(int argc, char** argv) {
    ros::init(argc, argv, "joystick_driver");
    joystick_controller jc;

    ros::spin();
}