# Servo Calibration

## Taking measurements
The servo motors need to be calibrated before using the robot. To do this, run the node `calibrate_servo.py` on the RPi and the `joy_node` on your laptop. This allows you to control the servos using an xbox one controller. Cycle through the servos using the cross-buttons, control the pulse width using the joystick y-axis.

```sh
# pi
sensors
rosrun servo_driver calibrate_servo.py

# laptop
rosrun joy joy_node
```

## Results
The results are calculated for you and placed in a yaml config file in the catkin ws. The idea is that this file can be loaded in the parameter server and the calibrations can be used in packages.
