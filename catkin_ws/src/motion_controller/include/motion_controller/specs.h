#ifndef SPECS_H
#define SPECS_H

// unit for all values in m
const double robotLength = 0.2075;
const double robotWidth = 0.078;
const double shoulderWidth = 0.1;

const double l1 = 0.0605;
const double l2 = 0.01;
const double l3 = 0.110832;
const double l4 = 0.13383;

#endif