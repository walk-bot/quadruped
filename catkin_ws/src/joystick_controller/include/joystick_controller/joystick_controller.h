#include <geometry_msgs/Twist.h>
#include <ros/ros.h>
#include <sensor_msgs/Joy.h>

#ifndef JOYSTICK_CONTROLLER_H
#define JOYSTICK_CONTROLLER_H

class joystick_controller {
   private:
    void joyCallback(const sensor_msgs::Joy::ConstPtr& joy);
    ros::NodeHandle nh_;

    int x_, y_, z_, linear_, roll_, pitch_, yaw_;

    double l_scale_, a_scale_;
    ros::Publisher vel_pub_;
    ros::Subscriber joy_sub_;

   public:
    joystick_controller();
};

#endif