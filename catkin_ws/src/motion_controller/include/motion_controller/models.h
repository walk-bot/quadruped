
#ifndef MODELS_H
#define MODELS_H

struct Vector3 {
    double x;
    double y;
    double z;

    // addition
    Vector3 operator+(const Vector3& v) {
        return {
            .x = x + v.x,
            .y = y + v.y,
            .z = z + v.z,
        };
    }

    Vector3 operator-(const Vector3& v) {
        return {
            .x = x - v.x,
            .y = y - v.y,
            .z = z - v.z,
        };
    }

    // scaling by constant factor
    Vector3 operator*(const double& lambda) {
        return {.x = x * lambda, .y = y * lambda, .z = z * lambda};
    }

    // addition assignment
    Vector3& operator+=(const Vector3& v) {
        x += v.x;
        y += v.y;
        z += v.z;
        return *this;
    }

    const double norm() { return {sqrt(pow(x, 2) + pow(y, 2) + pow(z, 2))}; }
};

using FPos = std::vector<Vector3>;

// "Kinemate" in german
struct Torsor {
    Vector3 v;
    Vector3 w;
};

#endif