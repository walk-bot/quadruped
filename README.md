# Quadruped

The goal of this project is to build and program a quadrupedal robot based on the [spot-micro](https://spotmicroai.readthedocs.io/en/latest/) hardware platform.

|                                     Walking (Video)                                     |                                 Sensor Feedback (Video)                                 |                           Inverse Kinematics Solver (Video)                            |
| :-------------------------------------------------------------------------------------: | :-------------------------------------------------------------------------------------: | :------------------------------------------------------------------------------------: |
| [![Video2](https://img.youtube.com/vi/nsYP71932Qo/0.jpg)](https://youtu.be/nsYP71932Qo) | [![Video2](https://img.youtube.com/vi/t9HcQKfcEus/0.jpg)](https://youtu.be/t9HcQKfcEus) | [![Video](https://img.youtube.com/vi/VqiOjQHmbGI/0.jpg)](https://youtu.be/VqiOjQHmbGI) |

- [Software Stack Installation](./docs/installation.md)
- [Sensor Configuration](./docs/sensors.md)
- [Servo Calibration](./docs/calibration.md)
- [progress updates](./docs/progress.md)

## Building

After cloning the repo you should first set an enviroment variable pointing to the catkin workspace `in catkin_ws`. Be sure to update the paths to reflect your own situation.

```sh
export CATKIN_WS=/home/$USER/git-repos/personal-repos/quadruped/catkin_ws
```

The project can be built with the following commands. The sim package will fail if you dont have raisim installed as outlined in the installation doc linked above. If you dont want to simulate then dont worry about the failed package build.

```sh
# For desktop / laptop
catkin build -DCMAKE_BUILD_TYPE=Release -DCMAKE_EXPORT_COMPILE_COMMANDS=1 -DCMAKE_CXX_STANDARD=14

# For RPI 4
catkin build -j4 -DCMAKE_BUILD_TYPE=Release -DCMAKE_CXX_STANDARD=14
```

## Usage

Source the executables in your terminal using
```sh
# assuming pwd is catkin_ws of this repo
. devel/setup.bash
```

Plug in a Xbox One Controller to your PC and run the following command. It will take joystick input, calculate joint angles for the servos and publish them as a topic.

```sh
roslaunch joystick_controller joystick_controller.launch
```

SSH into the Raspberry PI and then run the subscriber for the servo board driver that actually moves the legs:

```sh
rosrun servo_driver servo_driver.py
```

## Credits

The hardware is based on Michael Kubina's [spinoff](https://github.com/michaelkubina/SpotMicroESP32) of the original Spot-Micro, since he has kindly provided the FreeCAD files. Real nice for modding.
