#include <geometry_msgs/Point.h>
#include <ik_solver/ik_solver.h>
#include <ros/ros.h>
#include <sensor_msgs/Imu.h>
#include <sensor_msgs/JointState.h>
#include <std_msgs/Float64MultiArray.h>
#include <tf/transform_broadcaster.h>
#include <visualization_msgs/MarkerArray.h>

#include "models.h"
#include "specs.h"

#ifndef MOTION_CONTROLLER_H
#define MOTION_CONTROLLER_H

/**
 * Convention: times, distances, weights and basically everthing else
 * is given in SI units
 *
 * Angles are in radians
 *
 * Vectors are in the standard basis in R^3
 */
class motion_controller {
   private:
    ros::NodeHandle nh_;
    ros::Publisher joint_pub;
    ros::Publisher servo_pub;
    ros::Publisher vis_pub;

    ros::Subscriber imu_sub;
    ros::Subscriber joint_sub;
    ros::Subscriber odom_sub;
    ros::Subscriber vel_sub;

    tf::TransformBroadcaster broadcaster;

    sensor_msgs::JointState joint_state;
    geometry_msgs::TransformStamped odom_trans;
    visualization_msgs::MarkerArray markers;

    uint ctr = 0;
    double t = 0.0;  // t in [0, 1]

    const Torsor max_vel = {
        // [v] = m/s
        .v = {.x = 0.11, .y = 0.06, .z = 0},
        // [w] = rad
        .w = {.x = 2 * M_PI / 180, .y = 2 * M_PI / 180, .z = 10 * M_PI / 180}};
    Torsor baseLinkTorsor = {.v = {.x = 0.0, .y = 0.0, .z = 0.0},
                             .w = {.x = 0.0, .y = 0.0, .z = 0.0}};

    Vector3 rpy = {.x = 0.0, .y = 0.0, .z = 0.0};
    Vector3 displacement = {.x = 0.0, .y = 0.0, .z = 0.15};

    // stepHeight and step duration are constant, step length is dynamically
    // determined
    double t_step = 1.0;  // [t] = s
    Vector3 stepHeight = {.x = 0, .y = 0, .z = 0.06};

    double vel_norm = 0;

    // starting displacement for feet from body
    const FPos initPosition = {
        {.x = robotLength / 2, .y = shoulderWidth, .z = 0},
        {.x = -robotLength / 2, .y = shoulderWidth, .z = 0},
        {.x = -robotLength / 2, .y = -shoulderWidth, .z = 0},
        {.x = robotLength / 2, .y = -shoulderWidth, .z = 0},
    };
    FPos s0 = initPosition;
    FPos shoulders = initPosition;

    FPos s1 = s0;
    FPos s2 = s0;
    FPos fpos = s0;

    // encoding: 1 flight, 0 stance
    // stance phase is !flightPhase
    const std::vector<std::vector<int>> flightPhases = {
        {1, 0, 1, 0}, {0, 1, 0, 1}, {1, 0, 1, 0}, {0, 1, 0, 1}};

    std::vector<double> q_act = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};

    std::vector<double> servo_angles;
    std::vector<double> servo_min_limits = {-0.55,     -M_PI / 2, -M_PI / 2,

                                            -M_PI / 2, -M_PI / 2, -M_PI / 2,

                                            0.55,      -M_PI / 2, -M_PI / 2,

                                            -M_PI / 2, -M_PI / 2, -M_PI / 2};
    std::vector<double> servo_max_limits = {M_PI / 2, M_PI / 2, M_PI / 2,

                                            M_PI / 2,

                                            0.55,     M_PI / 2, M_PI / 2,

                                            0.55,     M_PI / 2, M_PI / 2};

    void onImuMsgReceived(const sensor_msgs::Imu::ConstPtr& imu_msg);
    void onJointAngleReceived(
        const std_msgs::Float64MultiArray::ConstPtr& q_msg);
    void onOdomReceived(const geometry_msgs::Point::ConstPtr& p);
    void onCmdVel(const geometry_msgs::Twist::ConstPtr& twst);

    void setDisplacement();
    Vector3 rotate_z(const Vector3& v, const double& phi);
    void moveLeg(const size_t& l, const uint& phase);
    void setShoulders();

    const FPos bezier(double t);

    void calculateServoAngles();
    std_msgs::Float64MultiArray servoAngleMsg();

    void setJointState();
    void setOdometry();
    void setMarker(const Vector3& v, const int& rgb);

   public:
    motion_controller(ros::NodeHandle nh);

    void nextTick();
    void publish();
};

#endif