import pybullet as p
import pybullet_data
import rospy
import os

from time import sleep
from std_msgs.msg import Float64MultiArray
from geometry_msgs.msg import Point
from sensor_msgs.msg import Imu

ws_path = os.environ["CATKIN_WS"]

useRealTimeSimulation = 1

q_d = [0.0038, -0.2549, -0.6830, 0.00384, -0.2549, -0.6830, -
       0.0038, -0.2549, -0.6830, -0.0038, -0.2549, -0.6830]

# revolute joint indices
revoluteJoints = [0, 1, 2, 4, 5, 6, 8, 9,
                  10, 12, 13, 14]

imu = Imu()
imu.header.frame_id = "base_link"

joint_pos = Float64MultiArray()
joint_pos.layout.dim = []

point = Point()


def joint_pos_callback(msg):
    global q_d
    q_d = msg.data


def send_imu_feedback():
    base_link_state = p.getLinkState(
        quad, 0, computeLinkVelocity=1
    )

    qx, qy, qz, qw = base_link_state[1]

    imu.header.stamp = rospy.Time.now()
    imu.orientation.x = qx
    imu.orientation.y = qy
    imu.orientation.z = qz
    imu.orientation.w = qw

    imu_pub.publish(imu)


def send_joint_pos_feedback():
    jointStates = p.getJointStates(quad, revoluteJoints)
    joint_pos.data = [state[0] for state in jointStates]

    joint_pos_pub.publish(joint_pos)


def send_odometry_feedback():
    point.x, point.y, point.z = p.getLinkState(quad, 0)[4]
    odom_pub.publish(point)


# integrate sim script with ROS
rospy.init_node("sim", anonymous=True)
rospy.Subscriber("/quad/joint_positions",
                 Float64MultiArray, joint_pos_callback)
imu_pub = rospy.Publisher('/quad/imu', Imu, queue_size=10)
joint_pos_pub = rospy.Publisher(
    '/quad/joint_position_feedback', Float64MultiArray, queue_size=10)
odom_pub = rospy.Publisher(
    '/quad/odom', Point, queue_size=10)

# connect headless sim client and initialize base world
p.connect(p.GUI)
p.setAdditionalSearchPath(pybullet_data.getDataPath())
p.loadURDF("plane.urdf")
p.setGravity(0, 0, -9.81)

# load the quadruped
cubeStartOrientation = p.getQuaternionFromEuler([0, 0, 0])
quad = p.loadURDF(
    ws_path + "/src/sim/src/quad.urdf", [0, 0, 0.25], cubeStartOrientation, flags=p.URDF_USE_INERTIA_FROM_FILE | p.URDF_USE_IMPLICIT_CYLINDER
)

if useRealTimeSimulation:
    p.setRealTimeSimulation(1)

# set a stable initial pose so that it doesnt fall over after spawning
p.setJointMotorControlArray(
    bodyUniqueId=quad,
    jointIndices=revoluteJoints,
    controlMode=p.POSITION_CONTROL,
    targetPositions=q_d,
)

while True:
    p.setJointMotorControlArray(
        bodyUniqueId=quad,
        jointIndices=revoluteJoints,
        controlMode=p.POSITION_CONTROL,
        targetPositions=q_d,
    )

    # send_imu_feedback()
    # send_joint_pos_feedback()
    # send_odometry_feedback()

    if useRealTimeSimulation:
        sleep(0.01)  # Time in seconds.
    else:
        p.stepSimulation()
