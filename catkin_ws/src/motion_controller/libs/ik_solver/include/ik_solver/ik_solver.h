#include <eigen3/Eigen/Geometry>
#include <vector>

#include "models.h"

#ifndef IK_SOLVER_H
#define IK_SOLVER_H

class ik_solver {
   private:
    Vector3 leg_inverse(const Eigen::Vector4f &p);
    Eigen::Vector4f leg_forward(const double &t1, const double &t2,
                                const double &t3);

    Vector3 rpy = {.x = 0, .y = 0, .z = 0};
    Vector3 midpoint = {.x = 0, .y = 0, .z = 0};

    Eigen::Matrix4f Tlf;
    Eigen::Matrix4f Tlb;
    Eigen::Matrix4f Trb;
    Eigen::Matrix4f Trf;
    Eigen::Matrix4f Ix;

    void convert(Vector3 &a, const bool &left);
    void setTransformationMatrices();

   public:
    ik_solver(const Vector3 &rpy, const Vector3 &midpoint) {
        this->rpy = rpy;
        this->midpoint = midpoint;

        this->setTransformationMatrices();
    }

    std::vector<double> getAngles(const std::vector<Vector3> &fpos);
    std::vector<Vector3> getPoints(const std::vector<double> &q);
};
#endif