#include <Adafruit_AHRS.h>
#include <Adafruit_Sensor_Calibration.h>
#include <Arduino.h>
#include <ros.h>
#include <sensor_msgs/Imu.h>
#include <std_msgs/Int16MultiArray.h>

#include <vector>

Adafruit_Sensor *accelerometer, *gyroscope, *magnetometer;
#include "NXP_FXOS_FXAS.h"

Adafruit_NXPSensorFusion filter;

sensors_event_t accel, gyro, mag;
float gx, gy, gz;
float qw, qx, qy, qz;

#define ADC_RESOLUTION 11
std::vector<int> joint_pos1;
std::vector<int> joint_pos2;
std::vector<int> joint_pos3;

sensor_msgs::Imu imu_msg;
std_msgs::Int16MultiArray joint_pos_msg;

ros::NodeHandle nh;
ros::Publisher imu_pub("/quad/imu", &imu_msg);
ros::Publisher joint_pos_pub("/quad/joint_position_feedback", &joint_pos_msg);

#if defined(ADAFRUIT_SENSOR_CALIBRATION_USE_EEPROM)
Adafruit_Sensor_Calibration_EEPROM cal;
#else
Adafruit_Sensor_Calibration_SDFat cal;
#endif

#define FREQ 300
#define PUB_EVERY_N_CYCLES 10

uint32_t timestamp;

void setPins() {
    pinMode(A14, INPUT);
    pinMode(A15, INPUT);
    pinMode(A16, INPUT);
    pinMode(A6, INPUT);
    pinMode(A3, INPUT);
    pinMode(A2, INPUT);
    pinMode(A9, INPUT);
    pinMode(A8, INPUT);
    pinMode(A7, INPUT);
    pinMode(A1, INPUT);
    pinMode(A0, INPUT);
    pinMode(A17, INPUT);
}

short angles[12];
/** Allocate memory for the joint position array message
 *
 * Most importantly, set message data pointer to global array that is
 * manipulated separately in analogread function. Things get messy otherwise
 */
void allocateMemory() {
    joint_pos_msg.layout.dim = (std_msgs::MultiArrayDimension *)malloc(
        sizeof(std_msgs::MultiArrayDimension));
    joint_pos_msg.layout.dim_length = 0;
    joint_pos_msg.data_length = 12;
    joint_pos_msg.layout.dim[0].label = "/quad/joint_position_feedback";
    joint_pos_msg.layout.dim[0].size = 12;
    joint_pos_msg.layout.dim[0].stride = 1 * 12;
    joint_pos_msg.data = angles;  // set pointer to angles)
}

void readJointAngles() {
    joint_pos3 = joint_pos2;
    joint_pos2 = joint_pos1; // to smooth out measurements, save older values for a rolling average

    joint_pos1 = {
        analogRead(A14), analogRead(A15), analogRead(A16), analogRead(A6),
        analogRead(A3),  analogRead(A2),  analogRead(A9),  analogRead(A8),
        analogRead(A7),  analogRead(A1),  analogRead(A17), analogRead(A0),
    };
}

void startIMU() {
    cal.begin();
    cal.loadCalibration();

    if (!init_sensors()) {
        while (1) delay(10);
    }

    setup_sensors();
    filter.begin(FREQ);
}

/** Read the motion sensors */
void readIMU() {
    accelerometer->getEvent(&accel);
    gyroscope->getEvent(&gyro);
    magnetometer->getEvent(&mag);

    cal.calibrate(mag);
    cal.calibrate(accel);
    cal.calibrate(gyro);

    gx = gyro.gyro.x * SENSORS_RADS_TO_DPS;
    gy = gyro.gyro.y * SENSORS_RADS_TO_DPS;
    gz = gyro.gyro.z * SENSORS_RADS_TO_DPS;

    filter.update(gx, gy, gz, accel.acceleration.x, accel.acceleration.y,
                  accel.acceleration.z, mag.magnetic.x, mag.magnetic.y,
                  mag.magnetic.z);

    filter.getQuaternion(&qw, &qx, &qy, &qz);
}

void setImuMsg(sensor_msgs::Imu &msg) {
    msg.header.stamp = nh.now();
    msg.header.frame_id = "base_link";

    // Filtered orientation quaternion
    msg.orientation.w = qw;
    msg.orientation.x = qx;
    msg.orientation.y = qy;
    msg.orientation.z = qz;

    // in m/s^2
    msg.linear_acceleration.x = accel.acceleration.x;
    msg.linear_acceleration.y = accel.acceleration.y;
    msg.linear_acceleration.z = accel.acceleration.z;

    // in rad/s
    msg.angular_velocity.x = gyro.gyro.x;
    msg.angular_velocity.y = gyro.gyro.y;
    msg.angular_velocity.z = gyro.gyro.z;
}

int average(int i){
   return (joint_pos1[i] + joint_pos2[i] + joint_pos3[i]) / 3;
}

void setJointAngleMsg() {
    for (size_t i = 0; i < joint_pos1.size(); i++) {
        angles[i] = average(i);
    }
}

void setup() {
    nh.initNode();

    allocateMemory();

    nh.advertise(imu_pub);
    nh.advertise(joint_pos_pub);

    setPins();
    startIMU();

    analogReference(DEFAULT);
    analogReadAveraging(32);  // take 32 samples before returning an estimate
    analogReadResolution(ADC_RESOLUTION);

    timestamp = millis();
    Wire.setClock(400000);  // 400KHz
}

void loop() {
    static uint8_t counter = 0;

    if ((millis() - timestamp) < (1000 / FREQ)) {
        return;
    }

    readIMU();
    timestamp = millis();

    // only print the calculated output once in a while
    if (counter++ <= PUB_EVERY_N_CYCLES) {
        return;
    }

    // reset the counter
    counter = 0;

    readJointAngles();
    setJointAngleMsg();
    joint_pos_pub.publish(&joint_pos_msg);

    setImuMsg(imu_msg);
    imu_pub.publish(&imu_msg);
    nh.spinOnce();
}
