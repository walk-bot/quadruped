from pandas import read_csv
from matplotlib import pyplot

series = read_csv('msgsp.csv', header=0, index_col=0,
                  parse_dates=True, squeeze=True, infer_datetime_format=True)

series.plot()
pyplot.xlabel('Time')
pyplot.ylabel('Joint angles')

pyplot.show()
