# Installation

## Master Node

### ROS
On your laptop or desktop computer install ROS Noetic.

[http://wiki.ros.org/noetic/Installation](http://wiki.ros.org/noetic/Installation)

### Environment Varialbes

Make sure environment variables for ROS are set

```sh
# ~/.zshrc or ~/.bashrc
export ROS_HOSTNAME=smolboi
export ROS_MASTER_URI=http://smolboi:11311

source /opt/ros/noetic/setup.bash
```

## Raisim

For simulation I use Raisim. Follow the latest installation guide on [raisim.com](https://raisim.com) The sim node will only link correctly against Raisim with set $WORKSPACE and $LOCAL_INSTALL environment variables.

## ROS

## Raspberry Pi 4

### Ubuntu with ROS Noetic on Raspberry Pi

Flash this image with ubuntu 20.04 server and ROS noetic preinstalled, and some swap space already configured.

[https://cloud.markmarolf.com/index.php/s/FLateAMb3LjomxJ](https://cloud.markmarolf.com/index.php/s/FLateAMb3LjomxJ)

Flash it to the SD card using gnome disks or something similar. Boot, do the usual SSH, Wifi Setup, set environment variables correctly for `ROS_MASTER_URI`

### I2C control

For the servo board driver to work, install the following packages.

'''sh
sudo pip3 install adafruit-circuitpython-pca9685

sudo pip3 install adafruit-io
sudo pip3 install RPI.GPIO
sudo pip3 install Adafruit-Blinka
sudo apt install python3-testresources
'''