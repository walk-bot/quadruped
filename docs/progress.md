# Progress

## 2022-02-11

Since I feel like the time has come for me to learn some formal fundamental robotics theory, I started doing a free online course from TU Darmstadt and will see how far I get in the next week of vacation. What is always striking after starting off projects fairly cluelessly (as was the case with the bluetooth indoor positioning system and now with the robot) and then seeing how it actually is done, is how close you can get with rudimentary, naive tools. Today I learned about the terminology, differential forward / inverse kinematics + dynamics and some basic control theory.

I have to change some stuff about the robot. Firstly, I should look into creating linear closed loop control (probably feed forward PD control) of the feet. For this I need joint encoders, since the system is open loop now with some janky smoothing.

Next up, I chose Bezier curves for Trajectory generation, which in hindsight should be replaced with quintic splines, since the control points are reached smoothly and also interpolate between velocities and accelerations (Bezier only did position, derivatives were handled by the easing functions). Shouldn't be too hard of a fix.

Lastly, I solved the inverse kinematics analytically using trigonometry and by fixing the base of the leg on a shoulder point. This works fine, so I think I wont worry too much about the Jacobian Pseudo-inverse stuff from the course. So far I didn't really have any problems with analytic IK. Yet still I think I should refactor the ik_solver class to compute the jacobian. I should see about using Eigen for my code instead of my custom vector classes, since I initially wanted to learn C++ but now dont want to implement all the linear algebra myself.

## 2022-02-05

Since the last post, I have tried out a few different simulation environments. I first did Webots, since a paper said it had an easy learning curve for new researchers and had good docs. The main problem was that the docs for the ROS integration were basically a link to a demo github-repo, which was sort of lame. In itself not a problem, because I could have just directly used their APIs. Yet more importantly, the URDF import to their proprietary model format didn't respect the scaling parameters of my Collada meshes. The robot parts were super messed up as a result and didnt seem to have an easy fix since the format was proprietary.

Next I worked on integrating Gazebo. URDF importing immediately worked and just required some work on the URDF. I had to get some semi-accurate values for the interia tensors on the links and more accurate values for joint friction and damping. The inertia part was easy: I just took some values from meshlab and smacked them in a spreadsheet with some math. The whole joint modelling part I am not so sure about, since the way I measured the joint friction and damping was pretty janky. I pushed a kitchen scale against a leg to measure the forces and moments. It remains to be seen how that will affect results.

Gazebo seems alright, but moving the robot in the simulator is not quite as I had imagined. I calculate servo angles (joint positions) that are directly sent to the robot. ROS control has a somewhat confusing ecosystem with all sorts of controllers (effort, position, velocity and subtypes), some with and without PID control. Since my motion controller supplies joint angles and has a discount version of PID control I made myself, I don't actually even need a fancy prebuilt ros controller. All I want is to make the legs move in the simulator.

Since ROS control didn't seem like the right match for my goals (I want to do as much as possible on my own to learn and apply stuff from ETH), I think I will just directly start off with Raisim, despite initially wanting to use Gazebo as a stepping stone.

Next steps will be to look at how real PID controllers work and get the robot to move in Raisim.

## 2022-01-26

This week I finished a model-based controller for the robot that makes it walk in software. I haven't tried deploying it on the physical robot yet as the controller isn't quite there yet. The contact scheduling for feet is not very convincing and the software model seems off, yet all seems easily fixable.

The next step for me is obviously the improvement of the model-based controller just to have seen it work on the actual robot. Afterwards there are two topics I am interested in. The first would be whole-body-motion control based on exteroceptive methods such as D-RGB cams or Lidar. The other option is to move forward with reinforcement learning and improve locomotion.

RL makes more sense in a few separate ways. Firstly, I looked online and saw how expensive the Lidar (RPLIDAR) and D-RGB (Intel Realsense) would be. Secondly it seems wrong to do whole-body motion planning optimization if you can't walk effectively in the first place. Then finally, I also believe there is a personal benefit to finally dipping my toes in the whole world of neural networks. I like the idea of RL because, according to my basic understanding, it doesn't require much labelling and supervision in generating data. So seems like an apt project to start with.

I read the paper from the RSL about RL and training for rough terrain but quickly noticed that I have no idea what they are talking about. Specifically, I have no clue how to apply it to my project. Their whole project data is on [github](https://github.com/leggedrobotics/learning_quadrupedal_locomotion_over_challenging_terrain_supplementary) though, which is helpful. I will also take this online course about [RL for Robotics](https://www.theconstructsim.com/robotigniteacademy_learnros/ros-courses-library/reinforcement-learning-for-robotics/)

