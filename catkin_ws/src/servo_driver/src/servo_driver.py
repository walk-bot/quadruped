import busio
import rospy

from board import SCL, SDA
from adafruit_pca9685 import PCA9685

from std_msgs.msg import Float64MultiArray


class Joint():
    # linear regression coefficients for angular displacement to pulse width
    # q(x) = slope_pw * x + intercept_pw
    slope_pw: float = 0.0
    intercept_pw: float = 0.0


i2c_bus = busio.I2C(SCL, SDA)
pca = PCA9685(i2c_bus, address=0x40)
pca.frequency = 330

joints = []


def loadparams():
    for i in range(12):
        joint = Joint()
        jointdict = rospy.get_param("/joints/j" + str(i))

        joint.slope_pw = jointdict['slope_pw']
        joint.intercept_pw = jointdict['intercept_pw']

        joints.append(joint)


def on_servo_angle(servo_angles):
    for i, q_d in enumerate(servo_angles.data):
        pca.channels[i].duty_cycle = int(
            q_d * joints[i].slope_pw + joints[i].intercept_pw) * 16


if __name__ == "__main__":
    rospy.init_node("servo_driver", anonymous=True)
    loadparams()

    rospy.Subscriber("/quad/joint_positions",
                     Float64MultiArray, on_servo_angle)

    rospy.spin()
